console.log('\n');

var bitcore = require('bitcore-lib');
var config = require('./ConfigPvt.json');

var privateKeyWIF = config.Alice_WIF;
var privateKey = bitcore.PrivateKey.fromWIF(privateKeyWIF);
var address = privateKey.toAddress();

console.log('Address: '+ address);
console.log(address);
