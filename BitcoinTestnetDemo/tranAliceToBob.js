console.log('\n');

// Common
var explorers = require('bitcore-explorers');
var bitcore = require('bitcore-lib');
var client = new explorers.Insight('testnet');
var configPvt = require('./ConfigPvt.json');
var configPub = require('./ConfigPub.json');

// Alice
var pvtKey_Alice = bitcore.PrivateKey.fromWIF(configPvt.Alice_WIF);
console.log('\n');

// Transaction
// Requires connection to the bitcore network

// Install bitcore-explorers
// npm install bitcore-explorers ---save
// The bitcore-explorers module provides a convenient interface to 
// retrieve unspent transaction outputs and broadcast transactions to the Bitcoin network via blockchain explorers.

// Send bitcoins from Alice to Bob
client.getUnspentUtxos(configPub.Alice_Add, function(err, utxos) { //Public key

    if(err){
        console.log('There is some problem broadcasting the transaction!');
    }else{

      UTXOs = utxos;
      console.log('UTXOs:', utxos);

      var tx = bitcore.Transaction();
      tx.from(utxos);
      tx.to(configPub.Bob_Add, 500000); //.005 BTC
      tx.change(configPub.Alice_Add);
      tx.fee(50000);
      tx.sign(pvtKey_Alice);
      //console.log('Transaction details: \n')
      //console.log(tx.toObject());

      tx.serialize();


        client.broadcast(tx.toString(), function(err, returnedTxId){
                if(err){
                        console.log('Broadcast error: '+err);
                }else{
                        console.log('Broadcast successful:, Transactio Id: '+returnedTxId);
                }
        });
    }
});
