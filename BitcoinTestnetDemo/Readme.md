1) When you get the following exception:

Error:
------------------------
C:\Users\pratmara\Documents\BlockChain\Exp\blockexperiments\BitcoinTestnetDemo\node_modules\bitcore-lib\index.js:12
    throw new Error(message);
    ^

Error: More than one instance of bitcore-lib found. Please make sure to require bitcore-lib and check that submodules do not also include their own bitcore-lib dependency.
    at Object.bitcore.versionGuard (C:\Users\pratmara\Documents\BlockChain\Exp\blockexperiments\BitcoinTestnetDemo\node_modules\bitcore-lib\index.js:12:11)
    at Object.<anonymous> (C:\Users\pratmara\Documents\BlockChain\Exp\blockexperiments\BitcoinTestnetDemo\node_modules\bitcore-lib\index.js:15:9)
    at Module._compile (module.js:643:30)
    at Object.Module._extensions..js (module.js:654:10)
    at Module.load (module.js:556:32)
    at tryModuleLoad (module.js:499:12)
    at Function.Module._load (module.js:491:3)
    at Module.require (module.js:587:17)
    at require (internal/module.js:11:18)
    at Object.<anonymous> (C:\Users\pratmara\Documents\BlockChain\Exp\blockexperiments\BitcoinTestnetDemo\tranAliceToBob.js:5:15)

Solution:   
--------------------------
line 7: in BitcoinTestnetDemo\node_modules\bitcore-lib\index.js

bitcore.versionGuard = function(version) {
Change it to:
bitcore.versionGuard = function(version) { return;

2) Get Bitcoins from the following testnet faucet 
https://testnet.coinfaucet.eu/en/
